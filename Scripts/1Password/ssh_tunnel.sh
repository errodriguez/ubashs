#!/usr/bin/env bash

# ssh_tunnel.sh v1.2.0
#
# This is a script that make use of 1Password CLI utilities to keep alive a
# SSH tunnel. SSH command, address and password are extracted from 1Password
# DB. Control session was initially based on OP_SESSION_account variable but
# its existence doesn't guarantees credentials are not to be requested if
# session expire.

###############################################################################
#                                                                             #
#                         Global Variables and functions                      # 
#                                                                             #
###############################################################################

function Usage {
    echo ""
    echo "Usage: $0 [-h]"
    echo ""
    echo "Options"
    echo "-h      Shows this help and exit (no matter other options)."
    echo ""
}

# Key for 1Password DB.
ITEM=""

###############################################################################
#                                                                             #
#                        Options and Call Verification                        #
#                                                                             #
###############################################################################

while getopts "h" opt
do
  case $opt in
    h|\?) # Show help and exit.
          Usage
          exit 1
          ;;
  esac
done
shift $((OPTIND -1))
 
if [[ $# > 0 ]]
   then Usage
        exit 2
fi

###############################################################################
#                                                                             #
#                               Initialisation                                # 
#                                                                             #
###############################################################################

# Main OP session is erased initially.
unset OP_SESSION_my

ITEM=airflow

###############################################################################
#                                                                             #
#                                  Process                                    # 
#                                                                             #
###############################################################################

while true
do 
   # To quit the loop check if some special key was pressed.
   read -n 1 -t 0.1 key
   case $key in
        q|Q|s|S) break
                 ;;
   esac

   # If main variable is set, avoid ask main password.
   if [ -z ${OP_SESSION_my+x} ] 
      then eval $(op signin)
	   echo "Connecting..."
   fi

   # But, we need to check if session is still valid.
   op get item $ITEM --fields command > /dev/null
   if [[ $? != "0" ]] 
      then unset OP_SESSION_my
	   continue
   fi

   # We get SSH command, address and password
   c=$(op get item $ITEM --fields command)
   a=$(op get item $ITEM --fields development)
   p=$(op get item $ITEM --fields password)

   # Just a proof of live.
   date

   # We make use of sshpass since SSH's SSH_ASSPASS seems to be a litle tricky.
   sshpass -p $p $c $a
done

###############################################################################
#                                                                             #
#                                Finalisation                                 # 
#                                                                             #
###############################################################################

unset OP_SESSION_my
exit 0
