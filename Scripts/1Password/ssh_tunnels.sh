#!/usr/bin/env bash

# ssh_tunnels.sh v2.1.2
#
# Base on ssh_tunnel.sh script. This script allows to extract one possible
# "address" of many registered in 1Password keyring to create an SSH tunnel.
# Record name and field to be queried should be passed to the script. 
# 1Password record should have from 1 to many address fields from where
# address for sshpass(1) command will be extracted.
#
# 1Password entry to query for a SSH tunnel shoud be a "login" record type and
# additionally it must include:
#   * 1 or more address por environment connection string, tagged with any name
#     (to be queried with ENV argument).
#   * A "command" field with the ssh command and parameters to use.
#   

###############################################################################
#                                                                             #
#                         Global Variables and functions                      # 
#                                                                             #
###############################################################################

function Usage {
    echo ""
    echo "Usage: $0 [-h] | ENTRY ENV"
    echo ""
    echo "OPTIONS"
    echo "-h         Shows this help and exit (no matter other options)."
    echo "ENTRY      Record to be queried in 1Password database."
    echo "ENV        Specific connection string tagged with name provided."
    echo ""
    echo "To gracefully quit from the script, just press 'q' or 's' key and it"
    echo "will exit in the next loop" 
    echo ""
    echo "EXAMPLE"
    echo ""
    echo ". ../ubashs/activate.sh"
    echo "$UBS/1Password/ssh_tunnels.sh ssh_tunnels eks"
    echo ""
}

# Keys for 1Password DB.
ITEM=""
LABEL=""

###############################################################################
#                                                                             #
#                        Options and Call Verification                        #
#                                                                             #
###############################################################################

while getopts "h" opt
do
  case $opt in
    h|\?) # Show help and exit.
          Usage
          exit 1
          ;;
  esac
done
shift $((OPTIND -1))
 
if [[ $# < 2 ]]
   then Usage
        exit 2
fi

###############################################################################
#                                                                             #
#                               Initialisation                                # 
#                                                                             #
###############################################################################

# Main OP session is erased initially.
unset OP_SESSION_my

ITEM=$1
LABEL=$2
PASSWORD=""

###############################################################################
#                                                                             #
#                                  Process                                    # 
#                                                                             #
###############################################################################

read -p "1Password's password: "  -s PASSWORD
echo ""

while true
do 
   # To quit the loop check if some special key was pressed.
   read -n 1 -t 0.1 key
   case $key in
        q|Q|s|S) break
                 ;;
   esac

   # If main variable is set, avoid ask main password.
   if [ -z ${OP_SESSION_my+x} ] 
      then eval $(op signin <<<$PASSWORD)
           if [ -z ${OP_SESSION_my+x} ] 
              then # Unauthorised
                   break
           fi
	   echo "Connecting..."
           if [[ $(op get item $ITEM --fields command 2>/dev/null) == "" ]] 
              then echo "Entry doent's have a 'command' field."
                   Usage
                   break
           fi
           if [[ $(op get item $ITEM --fields $LABEL 2>/dev/null) == "" ]]
              then echo "Unknown environment."
                   Usage
                   break
           fi
	   if [[ $(op get item $ITEM --fields password 2>/dev/null) == "" ]]
              then echo "Record doesn't have a 'password' field."
                   Usage
                   break
           fi
   fi

   # But, we need to check if session is still valid.
   op get item $ITEM --fields command > /dev/null
   case $? in
        0) # Everything is ok.
           ;;
        *) # Anything else.
           unset OP_SESSION_my
	   continue
           ;;
   esac

   # We get SSH command, address and password
   c=$(op get item $ITEM --fields command)
   a=$(op get item $ITEM --fields $LABEL)
   p=$(op get item $ITEM --fields password)

   # Just a proof of live.
   date

   # We make use of sshpass since SSH's SSH_ASSPASS seems to be a litle tricky.
   sshpass -p $p $c $a
done

###############################################################################
#                                                                             #
#                                Finalisation                                 # 
#                                                                             #
###############################################################################

unset OP_SESSION_my
exit 0
